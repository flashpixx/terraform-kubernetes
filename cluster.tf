locals {
  registryHostIntern    = join(":", [docker_container.registry.name, "5000"])
  registryHostExtern = join(":", ["localhost", tostring(var.registry_port)])
}


resource "kind_cluster" "k8s" {
  depends_on      = [docker_network.k8s]
  name            = var.name
  node_image      = var.cluster_image
  wait_for_ready  = true
  kubeconfig_path = var.kube_config

  kind_config {
    kind        = "Cluster"
    api_version = "kind.x-k8s.io/v1alpha4"

    containerd_config_patches = [
      join("", [
        "[plugins.\"io.containerd.grpc.v1.cri\".registry.mirrors.\"", local.registryHostExtern,"\"]\n  endpoint = [\"http://", local.registryHostIntern, "\"]"
      ])
    ]


    networking {
      api_server_address = var.cluster_bind_address
      api_server_port = var.cluster_bind_port
    }

    node {
      role = "control-plane"

      kubeadm_config_patches = [
        yamlencode({
          "kind" : "InitConfiguration",
          "nodeRegistration" : {
            "kubeletExtraArgs" : {
              "node-labels" : "ingress-ready=true"
            }
          }
        })
      ]


      dynamic "extra_port_mappings" {
        for_each = var.cluster_node_ports

        content {
          container_port = extra_port_mappings.value
          host_port      = extra_port_mappings.value
        }
      }
    }

    dynamic "node" {
      for_each = range(var.cluster_nodes)

      content {
        role = "worker"

        dynamic "extra_mounts" {
          for_each = var.cluster_mounts

          content {
            host_path      = extra_mounts.value.host_path
            container_path = extra_mounts.value.container_path
          }
        }
      }
    }
  }
}

resource "null_resource" "ingress" {
  depends_on = [kind_cluster.k8s]

  provisioner "local-exec" {
    command = join(" ", [
      "kubectl", "--kubeconfig", var.kube_config, "apply", "-f",
      "https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml"
    ])
  }
}

resource "null_resource" "ingress-wait" {
  depends_on = [null_resource.ingress]

  provisioner "local-exec" {
    command = join(" ", [
      "kubectl", "--kubeconfig", var.kube_config, "wait", "--namespace ingress-nginx", "--for=condition=ready", "pod",
      "--selector=app.kubernetes.io/component=controller", "--timeout=180s"
    ])
  }
}
