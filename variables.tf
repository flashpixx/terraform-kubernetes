variable "name" {
  description = "name prefix"
  type        = string
  default     = "tf"
}

variable "cluster_mounts" {
  description = "mountpoints of the local filesystem to bind in cluster"
  type = list(object({
    host_path      = string
    container_path = string
  }))
  default = []
}

variable "cluster_bind_address" {
  description = "ip bind address of the cluster"
  type        = string
  default     = "127.0.0.1"
}

variable "cluster_bind_port" {
  description = "port of the cluster configuration"
  type        = number
  default     = 6500
}

variable "cluster_image" {
  description = "kind cluster image"
  type        = string
  default     = "kindest/node:v1.29.0"
}

variable "cluster_node_ports" {
  description = "list of port mappings"
  type        = set(number)
  default     = [80]
}

variable "cluster_nodes" {
  description = "number of worker nodes"
  type        = number
  default     = 1
}

variable "registry_port" {
  description = "port of the registry"
  type        = number
  default     = 2200
}

variable "registry_image" {
  description = "docker image of the registry"
  type        = string
  default     = "registry:2"
}

variable "kube_config" {
  description = "path to generated kube config file"
  type        = string
  default     = ".kubeconfig"
}
