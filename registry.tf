resource "docker_network" "k8s" {
  name = "kind"
  driver = "bridge"
}

resource "docker_container" "registry" {
  name  = join("-", [var.name, "registry"])
  image = var.registry_image

  networks_advanced {
    name = docker_network.k8s.name
  }

  ports {
    internal = 5000
    external = var.registry_port
  }

  lifecycle {
    ignore_changes = [command, cpu_shares, dns, dns_opts, dns_search]
  }
}

resource "null_resource" "registry-wait" {
  depends_on = [docker_container.registry]

  provisioner "local-exec" {
    command = join("", ["until curl -k -s http://", local.registryHostExtern, " >/dev/null; do sleep 5; done"])
  }
}
