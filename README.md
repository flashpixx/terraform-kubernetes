# Terraform - Kubernetes KinD Cluster - Container Registry

The [Terraform](https://www.terraform.io/) script deploys a container [Docker Registry](https://docs.docker.com/registry/) and a [KinD](https://kind.sigs.k8s.io/docs/user/quick-start/) cluster with an [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) controller.

## Requirements

* [KinD](https://kind.sigs.k8s.io/docs/user/quick-start/) 
* [Kubectl](https://kubernetes.io/docs/reference/kubectl/)
* [Docker](https://www.docker.com/products/docker-desktop/)

All tools should be installed and accessable via `PATH` environment

### UI-Tools

* [K9s](https://k9scli.io/)
* [Headlamp](https://www.headlamp.dev/)

## Usage

Run

```
terraform init
terraform apply
docker ps
export KUBECONFIG=$PWD/.kubeconfig
```

Run your tool to get access

### Environment

See [variables.tf](./variables.tf) to see all settings

+ Cluster API is bind to `localhost:6500`
* Port `80` is passed to the ingress controller
* Container Registry is bind to `localhost:2200`
* 1 Master node and 1 Worker Node will be created
